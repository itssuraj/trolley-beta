Rails.application.routes.draw do
devise_for :users
  get "loadings/index"
  

  authenticated :user do
    root "loadings#index", as: "authenticated_root"
  end

  resources :loadings do
    resources :approved_trucks 
  end

  resources :trucks
  
  root "welcome#index"

end




