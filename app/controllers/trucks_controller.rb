class TrucksController < ApplicationController
	before_action :find_truck, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user!, except: [:index, :show]

	def index
		if current_user.role_id==2
			@trucks = Truck.where(user_id: current_user)
		else
			redirect_to root_path
		end
	end

	def show

	end

	def new
		if current_user.role_id==2
			@truck = current_user.trucks.build
		else
			redirect_to root_path
		end
	end
	
	def create
		if current_user.role_id==2
			@truck = current_user.trucks.build(truck_params)

			if @truck.save
				redirect_to @truck
			else
				render 'new'
			end
		else
			redirect_to root_path
		end
	end

	def edit
	end

	def update
		if current_user.role_id==2
			if @truck.update(truck_params)
				redirect_to @truck
			else
				render 'edit'
			end
		else
			redirect_to root_path	
		end
	end

	def destroy
		if current_user.role_id==2
			@truck.destroy
			redirect_to trucks_path
		else
			redirect_to root_path
		end
	end

	private

	def find_truck
		@truck = Truck.find(params[:id])
	end

	def truck_params
		params.require(:truck).permit(:truck_number)
	end

end
