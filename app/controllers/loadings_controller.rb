class LoadingsController < ApplicationController

	before_action :find_loading, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user!, except: [:index, :show]

	before_action :is_signed_in?


	def is_signed_in?
	   if !user_signed_in?
	      redirect_to new_user_session_path
	   end
	end


	def index
		if current_user.role_id==1
			@loadings = Loading.where(user_id: current_user)
		else
			@loadings = Loading.all.order("created_at DESC")
		end
	end

	def show
		@approved_trucks = ApprovedTruck.where(loading_id: @loading)
	end

	def new
		if current_user.role_id==1
			@loading = current_user.loadings.build
		else
			redirect_to root_path
		end
	end

	def create
		if current_user.role_id==1
			@loading = current_user.loadings.build(loading_params)

			if @loading.save
				redirect_to @loading
			else
				render 'new'
			end
		else
			redirect_to root_path
		end
	end

	def edit
	end

	def update
		if current_user.role_id==1
			if @loading.update(loading_params)
				redirect_to @loading
			else
				render 'edit'
			end
		else
			redirect_to root_path
		end
	end

	def destroy
		if current_user.role_id==1
			@loading.destroy
			redirect_to root_path
		else
			redirect_to root_path
		end
	end

	private

	def find_loading
		@loading = Loading.find(params[:id])
	end

	def loading_params
		params.require(:loading).permit(:from, :to, :weight, :goods_type, :number_of_trucks, :date_of_loading)
	end

end
