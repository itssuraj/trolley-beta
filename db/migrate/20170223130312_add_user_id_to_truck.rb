class AddUserIdToTruck < ActiveRecord::Migration[5.0]
  def change
    add_column :trucks, :user_id, :integer
  end
end
