class AddDateOfLoadingToLoading < ActiveRecord::Migration[5.0]
  def change
    add_column :loadings, :date_of_loading, :date
  end
end
