class AddUserIdToApprovedTruck < ActiveRecord::Migration[5.0]
  def change
    add_column :approved_trucks, :user_id, :integer
  end
end
