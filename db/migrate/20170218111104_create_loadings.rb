class CreateLoadings < ActiveRecord::Migration[5.0]
  def change
    create_table :loadings do |t|
      t.string :from
      t.string :to
      t.float :weight
      t.string :goods_type

      t.timestamps
    end
  end
end
